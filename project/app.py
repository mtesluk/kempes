from flask import Flask, jsonify, request, send_from_directory
import subprocess

app = Flask(__name__)

STATION = 0
CARRIAGE = 1
SIGNAL = 2

@app.route('/api/execute', methods=['POST', 'GET'])
def execute():
    data = request.json
    exec = {
        '0': 'skr1.py',
        '1': 'skr2.py',
        '2': 'skr3.py'
    }
    try:
        print(data)
        # subprocess.check_output(['python', exec[data['type']]])
    except Exception as e:
        return (jsonify({'message': 'Error occured'}), 500)
    return (jsonify({'message': 'Data updated'}), 200)

@app.route('/api/commons', methods=['GET'])
def commons():
    data = [
        {
            'x': 1,
            'y': 2,
            'name': 'name1',
            'fullName': 'fullname1',
            'type': 'station'
        },
        {
            'x': 11,
            'y': 62,
            'name': 'name3',
            'fullName': 'fullname3',
            'type': 'station'
        },
        {
            'x': 13,
            'y': 25,
            'name': 'name2',
            'fullName': 'fullname2',
            'type': 'carriage'
        }
    ]
    return (jsonify(data), 200)

@app.route('/api/signals', methods=['GET'])
def signals():
    data = [
        {
            'power': '123',
            'from': 2,
            'to': 1,
        },
        {
            'power': '657',
            'from': 12,
            'to': 133,
        }
    ]
    return (jsonify(data), 200)

@app.route('/<path:path>', methods=['GET'])
def static_proxy(path):
  return send_from_directory('./dist/kempes/', path)

@app.route('/', methods=['GET'])
def root():
    return send_from_directory('./dist/kempes/', 'index.html')

if __name__ == '__main__':
    app.run(debug=True)