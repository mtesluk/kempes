import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { HttpClient } from '@angular/common/http'; 


interface Common {
  name: string;
  fullName: string;
  x: number;
  y: number;
  type: string;
}


interface Signal {
  power: string;
  from: string;
  to: number;
}


@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
  title = 'hehe';
  commonForm = new FormGroup({
    name: new FormControl(''),
    fullName: new FormControl(''),
    x: new FormControl(''),
    y: new FormControl('')
  });

  signalForm = new FormGroup({
    power: new FormControl(''),
    from: new FormControl(''),
    to: new FormControl('')
  });
  displayedColumnsSignal = ['power', 'from', 'to'];
  displayedColumnsCommon = ['name', 'fullName', 'x', 'y', 'type'];
  dataCommon: Common[] = [];
  dataSignal: Signal[] = [];

  constructor(
    private _http: HttpClient
  ) {}

  ngOnInit() {
    this.getCommon();
    this.getSignal();
  }

  onSubmit(form: FormGroup, type) {
    this._http.post<any>('api/execute', {...form.value, type}).subscribe(
      response => {
        this.getData(type);
        form.reset();
      },
      error => alert(error.error.message));
  }

  getData(type: string) {
    type !== 'signal' ? this.getCommon() : this.getSignal();
  }

  getCommon() {
    this._http.get<Common[]>('api/commons').subscribe(resposne => {
      this.dataCommon = resposne;
    })
  }

  getSignal() {
    this._http.get<Signal[]>('api/signals').subscribe(resposne => {
      this.dataSignal = resposne;
    })
  }


}
